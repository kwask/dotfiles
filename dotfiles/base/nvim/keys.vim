" map      : map recursively
" noremap  : map non-recursively
" inoremap : map only in insert mode
" nnoremap : map only in normal mode

" set the character used for <leader> tags
let mapleader=" "

" force save with permissions
cnoremap w!! w !sudo tee % >/dev/null

" INSERT MODE MAPPINGS
" use jk to exit insert mode 
inoremap jk <C-c>

" NORMAL MODE MAPPINGS
nnoremap <C-s>                  :w<cr>
nnoremap <C-q>                  :q<cr>

nnoremap <silent><Tab>          :NERDTreeToggle<cr>
nnoremap <silent><leader>f      :Files<cr>
nnoremap <silent><leader>t      :TagbarToggle<cr>
nnoremap <silent><leader>nb     :set relativenumber!<cr>
nnoremap <silent><leader><cr>   :noh<cr>

nnoremap <leader>rt             :TestFile<cr>
nnoremap <leader>rtn            :TestNearest<cr>
nnoremap <leader>rts            :TestSuite<cr>

source ~/.config/nvim/functions.vim
nnoremap <silent>vA             :call SelectAll()<cr>
nnoremap <C-s>                  :w<cr>
nnoremap <C-q>                  :q<cr>
nnoremap <silent>Y              :call YankToLineEnd()<cr>
nnoremap <silent>D              :call DeleteToLineEnd()<cr>
nnoremap <leader>r              :source ~/.config/nvim/init.vim<cr>
map      <F7>                   :call Reindent()<cr>

" j and k move to next visual row instead of next text line
nnoremap j gj
nnoremap k gk

" tab controls
" switching tabs
nnoremap <leader>1              1gt<cr>
nnoremap <leader>2              2gt<cr>
nnoremap <leader>3              3gt<cr>
nnoremap <leader>4              4gt<cr>
nnoremap <leader>5              5gt<cr>
nnoremap <leader>6              6gt<cr>
nnoremap <leader>7              7gt<cr>
nnoremap <leader>8              8gt<cr>
nnoremap <leader>9              9gt<cr>
" other tab commands
nnoremap <leader>tn             :tabnew<cr>
nnoremap <leader>tc             :tabclose<cr>
nnoremap <leader>tm             :tabmove<cr>
nnoremap <leader>t.             :tabnext<cr>
nnoremap <leader>t,             :tabprevious<cr>

" BREAKING OLD HABITS
nnoremap <Left>  <nop>
nnoremap <Right> <nop>
nnoremap <Up>    <nop>
nnoremap <Down>  <nop>
inoremap <Left>  <nop>
inoremap <Right> <nop>
inoremap <Up>    <nop>
inoremap <Down>  <nop>
inoremap <esc>   <nop>
