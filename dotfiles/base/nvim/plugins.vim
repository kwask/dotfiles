call plug#begin('~/.local/share/nvim/plugs')
source ~/.config/nvim/plugins/coc.vim
source ~/.config/nvim/plugins/ale.vim
Plug 'rust-lang/rust.vim'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'flazz/vim-colorschemes'
Plug 'noah/vim256-color'
Plug 'majutsushi/tagbar'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-colorscheme-switcher'
Plug 'taverius/vim-colorscheme-manager'
Plug 'vim-test/vim-test'
call plug#end()
