" install plugin
Plug 'w0rp/ale'

" === ALE settings ===
let g:ale_linters = {
    \'rust': ['cargo'],
\}

let g:ale_rust_cargo_check_all_targets = 1
let g:ale_rust_cargo_use_check = 1
let g:ale_lint_on_save = 1