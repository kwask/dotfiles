" hemisu, boltzmann
colorscheme boltzmann

set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1

" if we're in gui mode, use custom font
if has('gui_running')
    set guifont=Inconsolata\ 14
endif

" cursor config
highlight Cursor guifg=white guibg=black
highlight iCursor guifg=white guibg=steelblue
set guicursor=n-v-c:block-Cursor
set guicursor+=i:ver100-iCursor
set guicursor+=n-v-c:blinkon0
set guicursor+=i:blinkwait10
